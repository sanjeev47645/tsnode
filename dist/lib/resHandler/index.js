"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const exceptionHandler_1 = __importDefault(require("./exceptionHandler"));
const constant_1 = require("../constant");
class resHandler {
    constructor() { }
    apiResponce(status, result) {
        if (status) {
            return { status, res: result };
        }
        else {
            return { status, err: result };
        }
    }
    handleError(res, result) {
    }
    sendSuccess(res, result) {
        let responce = this.apiResponce(constant_1.constant.SUCCESS, result);
        res.send(responce);
    }
    sendError(res, err) {
        if (!err.errCode)
            err = exceptionHandler_1.default.intrnlSrvrErr();
        let responce = this.apiResponce(constant_1.constant.ERROR, err);
        res.send(responce);
    }
}
exports.default = new resHandler;
