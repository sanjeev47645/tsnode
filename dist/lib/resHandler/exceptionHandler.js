"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const status_code_json_1 = __importDefault(require("./../constant/status_code.json"));
class exception {
    constructor() { }
    getProp(obj, key) {
        return obj[key];
    }
    exception(errCode, msg, err = {}) {
        if (err) {
            return { errCode, msg, err };
        }
        else {
            return { errCode, msg };
        }
    }
    intrnlSrvrErr() {
        return this.exception(status_code_json_1.default.internal_server_err.code, status_code_json_1.default.internal_server_err.msg);
    }
    customException(type, err = false) {
        let code = this.getProp(status_code_json_1.default, type).code, msg = this.getProp(status_code_json_1.default, type).msg;
        if (err)
            return this.exception(code, msg);
        else
            return this.exception(code, msg, err);
    }
}
exports.default = new exception();
