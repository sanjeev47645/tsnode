"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.expressConfig = void 0;
const helmet_1 = __importDefault(require("helmet"));
const body_parser_1 = __importDefault(require("body-parser"));
const method_override_1 = __importDefault(require("method-override"));
const response_time_1 = __importDefault(require("response-time"));
class expressConfig {
    constructor(app, env) {
        this._expConfig(app, env);
    }
    _expConfig(app, env) {
        app.use(response_time_1.default());
        app.use(helmet_1.default());
        app.use(body_parser_1.default.json());
        app.use(body_parser_1.default.urlencoded({ extended: false }));
        app.use(method_override_1.default((req) => {
            if (req.body && typeof req.body === "object" && "_method" in req.body) {
                var method = req.body._method;
                delete req.body._method;
                return method;
            }
        }));
        // app.use("/apiDocs",express.static(app.locals.rootdir+'/views'));
        app.use((req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Credentials", 'true');
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization, accessToken");
            res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
            if (req.method == 'OPTIONS') {
                res.status(200).end();
            }
            else {
                next();
            }
        });
        app.listen(env.port || 3000, () => {
            console.log(`at port ${env.port} server is running...`);
        });
    }
}
exports.expressConfig = expressConfig;
