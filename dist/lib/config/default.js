"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
exports.default = {
    TOKEN_EXPIRATION_SEC: 60 * 24 * 60 * 60,
    JWT_SECRET_KEY: 'g8b9(-=~Sdf)',
    uploadDir: path_1.default.resolve('./uploads'),
    basicAuth: {
        user: "tsDemo",
        pass: "tsDemo"
    }
};
