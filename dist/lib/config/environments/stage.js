"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stage = void 0;
exports.stage = {
    type: 'stage',
    port: 5000,
    mongo: {
        dbName: "",
        dbUrl: "",
        dbPort: "",
        options: {
            user: "",
            pass: "",
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true
        }
    }
};
