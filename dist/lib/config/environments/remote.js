"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.remote = void 0;
exports.remote = {
    type: 'remote',
    port: 5000,
    mongo: {
        dbName: "",
        dbUrl: "",
        dbPort: "",
        options: {
            user: "",
            pass: "",
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true
        }
    }
};
