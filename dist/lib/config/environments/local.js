"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.local = void 0;
exports.local = {
    type: 'local',
    port: 5000,
    mongo: {
        dbName: "tsDb",
        dbUrl: "mongodb://localhost",
        dbPort: "27017",
        options: {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true
        }
    }
};
