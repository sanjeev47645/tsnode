"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dev = void 0;
exports.dev = {
    type: 'dev',
    port: 5000,
    mongo: {
        dbName: "",
        dbUrl: "",
        dbPort: "",
        options: {
            user: "",
            pass: "",
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true
        }
    }
};
