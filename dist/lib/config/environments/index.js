"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Env = void 0;
const dev_1 = require("./dev");
const prod_1 = require("./prod");
const remote_1 = require("./remote");
const local_1 = require("./local");
const stage_1 = require("./stage");
class Env {
    constructor(type, cfg = {}) {
        this.cfg = cfg;
        this.selectOption(type);
    }
    selectOption(type) {
        switch (type) {
            case 'dev':
                this.cfg = dev_1.dev;
                break;
            case 'prod':
                this.cfg = prod_1.prod;
                break;
            case 'stage':
                this.cfg = stage_1.stage;
                break;
            case 'local':
                this.cfg = local_1.local;
                break;
            case 'remote':
                this.cfg = remote_1.remote;
                break;
        }
    }
}
exports.Env = Env;
