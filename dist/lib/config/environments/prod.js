"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.prod = void 0;
exports.prod = {
    type: 'prod',
    port: 5000,
    mongo: {
        dbName: "",
        dbUrl: "",
        dbPort: "",
        options: {
            user: "",
            pass: "",
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true
        }
    }
};
