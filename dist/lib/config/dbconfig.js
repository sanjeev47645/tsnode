"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Db = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
class Db {
    constructor(env, cb) {
        this.env = env;
        this.cb = cb;
        this._connection(env, cb);
    }
    _connection(env, cb) {
        let uri = env.mongo.dbUrl, dbOptions = env.mongo.options, dbName = env.mongo.dbName, dbPort = env.mongo.dbPort;
        if (env.type == 'prod') {
        }
        else {
            mongoose_1.default.set('debug', true);
        }
        uri = `${uri}/${dbName}:${dbPort}`;
        console.log(`Connecting to -> ${uri}`);
        mongoose_1.default.connect(uri, dbOptions).then(() => {
            console.log(`Connected to Db ${dbName} at ${dbPort}`);
        }).catch(err => {
            console.log(`DB connection error: ${err}`);
            cb(err);
        });
    }
}
exports.Db = Db;
