"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.adminRouter = void 0;
const express_1 = require("express");
const controller_1 = __importDefault(require("./controller"));
const resHandler_1 = __importDefault(require("../../resHandler"));
exports.adminRouter = express_1.Router();
exports.adminRouter.route('/login').get([], (req, res) => {
    let info = req.query;
    controller_1.default.login(info).then(result => {
        resHandler_1.default.sendSuccess(res, result);
    }).catch(err => {
        resHandler_1.default.sendError(res, err);
    });
});
