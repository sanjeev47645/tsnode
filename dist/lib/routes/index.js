"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Route = void 0;
const basicAuth_1 = require("./../middleware/basicAuth");
const route_1 = require("./../modules/admin/route");
const resHandler_1 = __importDefault(require("./../resHandler"));
class Route {
    constructor(app) {
        this.allRoute(app);
    }
    allRoute(app) {
        app.use(basicAuth_1.basicAuth);
        app.use('/api/v1/admin', route_1.adminRouter);
        app.use(resHandler_1.default.handleError);
    }
}
exports.Route = Route;
