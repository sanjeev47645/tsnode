"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.basicAuth = void 0;
const basic_auth_1 = __importDefault(require("basic-auth"));
const config_1 = __importDefault(require("./../config"));
exports.basicAuth = (req, res, next) => {
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    }
    var credentials = basic_auth_1.default(req);
    if (!credentials || credentials.name !== config_1.default.basicAuth.user || credentials.pass !== config_1.default.basicAuth.pass) {
        res.statusCode = 401;
        res.setHeader('WWW-Authenticate', 'Basic realm="example"');
        res.send({
            message: 'Access denied'
        });
    }
    else {
        next();
    }
};
