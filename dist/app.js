"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dbconfig_1 = require("./lib/config/dbconfig");
const expressconfig_1 = require("./lib/config/expressconfig");
const config_1 = __importDefault(require("./lib/config"));
const routes_1 = require("./lib/routes");
const app = express_1.default();
new dbconfig_1.Db(config_1.default, (err) => {
    console.log(err);
});
new expressconfig_1.expressConfig(app, config_1.default);
app.locals.rootdir = __dirname;
new routes_1.Route(app);
