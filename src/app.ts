import express,{Application} from 'express';
import {Db} from './lib/config/dbconfig';
import {expressConfig} from './lib/config/expressconfig';
import config from './lib/config';
import { Route } from './lib/routes';
import {logger} from './lib/logger'
const app:Application=express();
app.locals.rootdir=__dirname;
new Db(config,(err:never)=>{
    if(err)
    logger.error(err,'Exiting the app')
    console.log(err)
    return;
})
new expressConfig(app,config)
new Route(app);
