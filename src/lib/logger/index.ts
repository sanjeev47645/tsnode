import bunyan from 'bunyan';

import config from './../config';
const bunyanDebugStream = require('bunyan-debug-stream');
let streams:Array<Object>=[];
if(config.type=='prod')
    streams.push({
        level: 'error',
        type: 'raw',
        stream:bunyanDebugStream({
            basepath: __dirname, // this should be the root folder of your project.
            forceColor: true
        })
    },
    {
        level: 'warn',
        type: 'raw',
        stream:bunyanDebugStream({
            basepath: __dirname, // this should be the root folder of your project.
            forceColor: true
        })
    })
    else 
    streams.push({
        level: 'error',
        path: 'app.log',
    },
    {
        level: 'warn',
        path: 'app.log',
    },
    {
        level: 'info',
        path: 'app.log',
    }
    )
export const logger=bunyan.createLogger({
    name:'TS demo',
    streams,
    serializers: {
        req: bunyan.stdSerializers.req,
        res: bunyan.stdSerializers.res,
        err: bunyan.stdSerializers.err
    }

})
