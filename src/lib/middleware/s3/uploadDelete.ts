 import  Promise from "bluebird";
import  AWS from 'aws-sdk';
import fs  from "fs"
import path from "path"
import config from './../../config';


AWS.config.update({
    accessKeyId: config.awsIamUser.accessKeyId,
    secretAccessKey: config.awsIamUser.secretAccessKey,
    region: config.s3.region
});
const Bucket = config.s3.bucketName;
const s3Bucket=new AWS.S3({
    accessKeyId: config.awsIamUser.accessKeyId,
    secretAccessKey: config.awsIamUser.secretAccessKey,
})


class uploadDelete{
    constructor(){}
    deleteTempFile(filepath:string){
        fs.stat(filepath,(err:any,stats:any)=>{
            if(err) console.log(err);
            fs.unlink(filepath,(err:any)=>{
                if(err) console.log(err);
                console.log('file deleted from server')
            })
        })
    }
    uploadToS3(file:any,buffer:any,filekey:string):Promise<any>{
        let params={
            Bucket: Bucket,
            Key: filekey,   
            ContentType: file.mimetype || 'image/png',
            Body: buffer
        }
        return new Promise((resolve,reject)=>{
            s3Bucket.upload(params,(err:any,data:any)=>{
                buffer.destroy();
                if(err) reject(err);
                else resolve(data);
            })
        })

    }

    uploadFile(file:any,filename:string):Promise<any>{
        let buffer=fs.createReadStream(file);
        return this.uploadToS3(file,buffer,filename).then(data=>{
            console.log('file upload s3 adn calling delete from server');
            this.deleteTempFile(file.path)
            return data
        }).catch(err=>{
            console.log(err);
            this.deleteTempFile(file.path)
        })
    }

    deleteFromS3(fileKey:string){
        console.log('delete processing');
        s3Bucket.deleteObject({Key:fileKey,Bucket},(err,data)=>{
            if(err){
                console.log('delete fail',err);
                return false
            }else{
                console.log('deleted successfully');
            return true
            }
        })
    }
}

export default new uploadDelete 
