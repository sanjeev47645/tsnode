import fs from 'fs';
import {logger} from './../../logger';
import uploadDelete from './uploadDelete';
import {Request,Response,NextFunction} from 'express'
import config from './../../config';
import appUtils from './../../services/appUtils';
import {Promise} from 'bluebird';

class mediaUpload{
    constructor(){}
    getFileKey(key:string,name:string):string{
        let filekey;
        switch(key){
            case "usr_img":
                return  config.s3uploadPath.user.profile + name
            default:
               return config.s3uploadPath.user.profile+name
        }

    }
    uploadSingleMediaToS3(key:any,multikey:boolean=false){
        return (req:Request,res:Response,next:NextFunction)=>{
            let file:any=(req.files || req.file);
            if(file) return  next();
            if(appUtils.isObjEmp(file)) return next()
            if (multikey) {
                if (appUtils.varKeyObj(file,key))
                    file = appUtils.varKeyObj(file,key)[0];
                else
                    return next()
            }
            let fileKey = this.getFileKey(key, file.filename);
            return new Promise((resolve,reject)=>{
                return uploadDelete.uploadFile(file,fileKey).then(url=>{
                    console.log(url);
                    return next()
                }).catch(err=>{
                    console.log('Error while uploading to s3',err)
                    throw err;
                })
            })
        }
    }
    uploadMultiMediaToS3(key:any,multikey:boolean=false){
        return (req:Request,res:Response,next:NextFunction)=>{
            let files:any=req.files ;
            if(files) return  next();
            if(appUtils.isObjEmp(files)) return next()
            if (multikey) {
                if (appUtils.varKeyObj(files,key))
                    files = appUtils.varKeyObj(files,key);
                else
                    return next()
            }
            Promise.mapSeries(files,(file:any)=>{
                let filename=this.getFileKey(key,file.filename)
                return uploadDelete.uploadFile(file,filename).then(url=>{
                    console.log(url)
                    return next();
                }).catch(err=>{
                    console.log('Error while uploading images ',err);
                })
            })
        }
    }
}

export default new mediaUpload;