import multer from 'multer';
import fs from 'fs';
import config from './../config';
let path=config.cfg.uploadDir;
if (!fs.existsSync(path)) {
    fs.mkdir(path, { recursive: true },err=>{});
}
let fileName:string='';

let storage = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, path);
	},
	filename: function (request, file, callback) {

		var time = new Date().getTime();
		fileName = file.fieldname + '_' + time + '_' + file.originalname.slice(file.originalname.lastIndexOf('.') - 1);
		fileName=fileName.replace(/ /g,'');
		callback(null, fileName);
	}
});
const  upload = multer({storage});
 class Upload{
    constructor(){} 
     singleFile(key:string) {
        return upload.single(key);
    }
    
    fileArray(key:string, count:number) {
        return upload.array(key, count);
    }
    
     fields (arr:any) {
        return upload.fields(arr);
    }
}

export default new Upload