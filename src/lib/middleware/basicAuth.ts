import auth from 'basic-auth';
import {Request,Response,NextFunction} from 'express';
import config from './../config';

export const basicAuth= (req:Request,res:Response,next:NextFunction)=>{
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    }
    var credentials = auth(req);
    if (!credentials || credentials.name !== config.basicAuth.user || credentials.pass !== config.basicAuth.pass) {
        res.statusCode = 401
        res.setHeader('WWW-Authenticate', 'Basic realm="example"')
        res.send({
            message: 'Access denied'
        })
    } else {
        next();
    }

}