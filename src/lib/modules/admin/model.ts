import mongoose,{Schema} from 'mongoose';
import {User} from './interface';


const useSchema:Schema = new Schema({
    name:String,
    email:{type:String,required:true,lowercase:true,index:true},
    hash:String,
    salt:String
})

export default mongoose.model<User>('user',useSchema);