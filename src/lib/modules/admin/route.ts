import express,{Router,Request,Response} from 'express';
import admin from './controller'
import resHandler from '../../resHandler';

export const adminRouter=Router();


adminRouter.route('/login').post([],(req:Request,res:Response)=>{
    let info=req.query;
    admin.login(info).then(result=>{
        resHandler.sendSuccess(res,result)
    }).catch(err=>{
        resHandler.sendError(res,err);
    })
})