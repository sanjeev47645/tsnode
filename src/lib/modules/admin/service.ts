import userModel from './model';

class User{
    constructor(){}
    login(info:any){
        let query={email:info.email}
       return userModel.findOne(query)
    }
    saveUser(info:Object):Promise<any>{
        let user=new userModel(info)
        return user.save(info);
    }
}
export default new User
