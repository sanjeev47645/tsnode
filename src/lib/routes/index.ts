import { Application } from "express";
import {basicAuth} from './../middleware/basicAuth';
import {adminRouter} from './../modules/admin/route';
import handler from './../resHandler'

export class Route{
    constructor(app:Application){
        this.allRoute(app)
    }
    allRoute(app:Application){
        app.use(basicAuth)
        app.use('/api/v1/admin',adminRouter)
        app.use(handler.defaultRoute)
        app.use(handler.handleError)
       
    }
}