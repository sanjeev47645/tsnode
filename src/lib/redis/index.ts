import config from './../config';
import {promisifyAll} from 'bluebird'
import redis from 'redis'
promisifyAll((<any>redis).RedisClient.prototype)
promisifyAll((<any>redis).Multi.prototype)
const  client=(<any>redis).createClient({...config.redis})
class Redis{
    constructor(){
        return client.onAsync('error').then((err:any)=>{
            console.log(err);
        })
    }
    setValue(key:string,value:any){
       return client.setAsync(key,value).then((res:any)=>{
        if(res){
            
            return res;
        }
       }).catch((err:any)=>{
           return err
       })
    }
    getValue(key:any){
            return client.getAsync(key).then((res:any)=> {
                return res;
            }).catch( (err:any) =>{
                return err;
            });
    }
    expire(key:any, expiryTime:any){
        return client.expireAsync(key, expiryTime).then( (res:any)=> {
            return res;
        }).catch( (err:any) =>{
           console.log(err)
    })
}
    deleteValue(key:any){
        return client.delAsync(key).then((res:any)=> {
            return res;
        }).catch(function (err:any) {
            throw err;
        });
    }
}

export default new Redis


