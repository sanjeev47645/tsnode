import * as dotenv from "dotenv";
dotenv.config();
import constant from './default';
import {Env} from './environments'
const env=process.env.NODE_ENV || 'local'
const envConfig=new Env(env);
// import {Db} from './dbconfig';
// import {expressConfig} from './expressconfig';

export default {...constant,...envConfig.cfg}