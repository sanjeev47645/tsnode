import express,{Application,Request,Response,NextFunction} from 'express'
import helmet from 'helmet'
import bodyParser  from 'body-parser'
import methodOverride from 'method-override'
import responseTime from 'response-time';
export class expressConfig {

    constructor(app:Application,env:any){
        
     this._expConfig(app,env);   
    }
    _expConfig(app:Application,env:any){
        app.use(responseTime())
        app.use(helmet());
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({extended:false}));

        app.use(methodOverride((req:Request)=>{
            if( req.body && typeof req.body === "object" && "_method" in req.body){
                    var method = req.body._method;
                    delete req.body._method;
                    return method;
            }
        }))
console.log(app.locals.rootdir+'/views/dist');
       app.use("/apiDocs",express.static(app.locals.rootdir+'/views/dist'));

        app.use((req:Request,res:Response,next:NextFunction)=>{
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Credentials",'true');
            res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept, authorization, accessToken");
            res.header("Access-Control-Allow-Methods","POST, GET, PUT, DELETE, OPTIONS");
            if(req.method=='OPTIONS'){
                res.status(200).end();
            }else{
                next()
            }
        })
        app.listen(env.port||3000,()=>{
            console.log(`at port ${env.port} server is running...` )
        })
    }
}