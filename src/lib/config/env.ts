export interface env{
    type:String,
    port:number
    mongo:{
        dbName: string,
        dbUrl: string,
        dbPort:string
        options: {
            user?: string,
            pass?:string ,
            useNewUrlParser: boolean,
            useCreateIndex: boolean,
            useUnifiedTopology: boolean
        }
    }
    redis:{
        server: String,
        port: Number,
        auth_pass: String
    }
}