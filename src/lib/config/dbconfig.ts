import mongoose from  'mongoose'
import {logger} from './../logger';
export class Db{
    constructor(public env:any,public cb:Function){
        this._connection(env,cb);
    }
    _connection(env:any,cb:Function){
        let uri=env.mongo.dbUrl,dbOptions=env.mongo.options,dbName=env.mongo.dbName,dbPort=env.mongo.dbPort;
        if(env.type=='prod'){
            logger.info("Configuring db in " + env.type + ' mode');
        }else{  
            logger.info("Configuring db in " + env.type + ' mode');
            mongoose.set('debug',true);
        }
        uri=`${uri}/${dbName}:${dbPort}`
        console.log(`Connecting to -> ${uri}`);
        mongoose.connect(uri,dbOptions).then(()=>{
            console.log(`Connected to Db ${dbName} at ${dbPort}`)
        }).catch(err=>{
            console.log(`DB connection error: ${err}`);
            cb(err)
        })
    }
}