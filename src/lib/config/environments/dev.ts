import {env} from './../env';
export const dev:env= {
    type:'dev',
    port:5000,
    mongo:{
        dbName:"",
        dbUrl:"",
        dbPort:"",
        options:{
            user:"",
            pass:"",
            useUnifiedTopology: true,
            useNewUrlParser:true,
            useCreateIndex:true
        }
    },
    redis:{
        server: 'localhost',
        port: 6379,
        auth_pass:""
    }
}