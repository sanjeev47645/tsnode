
import {dev} from './dev';
import {prod} from './prod'
import {remote} from './remote'
import {local} from './local'
import {stage} from './stage'
import {env} from '../env'

export class Env  {
    constructor(type:string,public cfg:any={}){
        this.selectOption(type)
    }
    selectOption(type:string){
        switch(type){
            case 'dev':
                this.cfg= dev;
                break;
            case 'prod':
                 this.cfg= prod;
                 break;
            case 'stage':
               this.cfg=stage;
               break;
            case 'local':
                this.cfg= local
                break;
            case 'remote':
                this.cfg= remote;
                break;
    }
    }
}