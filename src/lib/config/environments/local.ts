import {env} from '../env';
export const local:env= {
    type:'local',
    port:5000,
    mongo:{
        dbName:"tsDb",
        dbUrl:"mongodb://localhost",
        dbPort:"27017",
        options:{
            useUnifiedTopology: true,
            useNewUrlParser:true,
            useCreateIndex:true
        }
    },
    redis:{
        server: 'localhost',
        port: 6379,
        auth_pass:""
    }
}