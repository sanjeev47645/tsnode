 import {default as status_code} from './../constant/status_code.json'
 import {err} from './model/err'
 class exception{
    constructor(){}
    getProp<T,K extends keyof T>(obj:T,key:K){
        return obj[key];
    }
    exception(errCode:Number,msg:String,err:Object={}):err{
        if(err){
            return {errCode,msg,err}
        }else{
            return {errCode,msg}
        }
    }
    intrnlSrvrErr(){
    return this.exception(status_code.internal_server_err.code,status_code.internal_server_err.msg)        
    }
    customException(type:any,err=false):err{
        let code=this.getProp(status_code,type).code,msg=this.getProp(status_code,type).msg
        if(err)
        return this.exception(code,msg);
        else
        return this.exception(code,msg,err)
    }
}
export default new exception();