import {Response,Request} from 'express'
import exception from './exceptionHandler'
import {constant}from '../constant';
import {responce} from './model/responce'
 class resHandler{
    constructor(){}
    apiResponce(status:number,result:Object):responce{
      if(status){
        return {status,res:result}
      }else{
        return {status,err:result}
      }
    }
    handleError(res:Response,result:Object){
       this.sendError(res,result);
    }
    defaultRoute(req:Request,res:Response){
      res.send({jarvis:"Check the Api"})
    }
    sendSuccess(res:Response,result:Object){
        let responce=this.apiResponce(constant.SUCCESS,result)
        res.send(responce)
    }
    sendError(res:Response,err:any){
        if (!err.errCode) 
             err = exception.intrnlSrvrErr();
        let responce=this.apiResponce(constant.ERROR,err)
        res.send(responce)
    }
}

export default new resHandler;